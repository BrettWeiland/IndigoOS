# IndigoOS
IndigoOS (name likely to change) will be a Unix-like operating system for x86-64 computers. I'm hoping to implement many things a modern operating system would have, including a desktop environment, pre-emptive multithreading, ASLR, and memory protection. I'd also like to port games such as DOOM and Quake.

# Project scope
While I'm loving this project, I do want it to be "done" at some point so I can work on other things. Because of this, my OS will be very simple, monolithic, and may not have all the bells and whistles of other osdev projects. I'm not planning on implementing my own libc, and when I get to user space, will end up porting a few programs instead of writing my own (such as Xorg or QT). 
With all that out of the way, check out the features.

# Planned Features
* SMP multi-core support
* Preemptive multithreading
* VFS abstraction for filesystem modules
* Unix-like file structure (/proc, /sys, /dev)
* Port of ether X-org or wayland, combined with a complete WM
* Basic network stack
* ex2 filesystem built in
* Pseudoterminals
* Ports of other software I like, such as bash, vim, some old 3d games

# Project status
This project will likely go very slow and take multiple years, as I'm dealing with things such as college and adulting. If there are blatantly obvious bugs, or things look unfinished, I'm still working on them.
Additionally, there's some pretty messy code. I've improved while working on this project, so expect those things to be changed some time down the line.

# Just completed
1. palloc()/pfree() is now smp safe

# What I'm working on now
1. Ensuring malloc()/free() is smp safe (current)
2. Creating kernel space threads with primitive scheduler
3. VFS filesystem
4. ACPI, PCI
5. ext2 filesystem
6. Userspace processes!!!
7. IO buffering, process waitlists
8. Implement permanent scheduler
9. Let's not get too ahead of ourselves...

# Backlog
These are things I want to evencually fix, but don't effect current development.
1. Making the bootloader pass kernel command paramaters from fat filesystem. Bootloader has no filesystem and is part of kernel, this will be hard
2. Making mutexes more space efficent; right now we use a 16 bit variable for every boolean operation
