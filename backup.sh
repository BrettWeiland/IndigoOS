#!/bin/bash
if [ $# -ne 1 ]
then
 echo "Usage: $0 [file suffix]" 
 exit 1
fi

archive_name=$(date +%m.%d.%y."$1".tar)

cd src
make clean
cd ../

tar -cf "$archive_name" src &&
pigz -9 $archive_name


