#!/bin/bash
export PREFIX=/home/indigo/projects/small/IndigoOS/compiler/indigo_gcc
export TARGET=x86_64-elf
export PATH="$PREFIX/bin:$PATH"

export JOBS=7

mkdir -p indigo_gcc/
mkdir -p src/

mkdir src/packs
mkdir src/build_binutils
mkdir src/build_gcc



#download the latest binutils
lftp -c "set xfer:clobber on; connect ftp.gnu.org; get -O src/packs/ gnu/binutils/$(lftp -c 'connect ftp.gnu.org; ls gnu/binutils/binutils*.tar.gz' | awk '{print $NF}' | sort -V | tail -n 1)"

#download gcc
# TODO clean up this script so we only need one connection

latest_gcc=$(lftp -c 'connect ftp.gnu.org; ls -d gnu/gcc/gcc-*' | grep -E "^d" | awk '{print $NF}' | sort -V | tail -1)
lftp -c "set xfer:clobber on; connect ftp.gnu.org; get -O src/packs/ gnu/gcc/$latest_gcc/$latest_gcc.tar.gz"

cd src/packs/

for f in *tar.gz; do 
  tar -xf "$f"
done

cd ../build_binutils
../packs/binutils-*/configure --target=$TARGET --enable-interwork --enable-multilib --disable-nls --disable-werror --prefix="$PREFIX" &&
make -j$JOBS all install 2>&1 | tee binutils_build_log
make install

cd ../build_gcc
../packs/gcc-*/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --disable-libssp --enable-languages=c --without-headers &&
make -j$JOBS all-gcc &&
make -j$JOBS all-target-libgcc &&
make install-gcc
make install-target-libgcc
cd ../../
sudo rm -r src

