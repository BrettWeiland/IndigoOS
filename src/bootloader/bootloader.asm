[bits 16]
[extern _kernel_sector_size]
[extern _bootloader_stage1_size]

jmp stage0 
times 3-($-$$) db 0x90 
times 59 db 0          

stage0:

jmp 0:.entry
.entry:

mov ax, 0
mov ds, ax
mov es, ax

;stack segments
mov ax, 0x7000
mov ss, ax
mov ax, 0xfff0 
mov sp, ax
mov bp, 0

mov al, 0x92
or al, 2
out 0x92, al


call bios_disk.init


mov bx, .loadstage2_msg
call bios_print

.load_stage1:
;TODO LOAD STAGE ONE HERE

; we start on 0x100000
; esi: sector start
; cx: sector count
; edi: memory desination

mov esi, 1      ; 2nt sector
mov ecx, _bootloader_stage1_size
mov edi, 0x7e00 ; desination
call bios_disk.load_sectors_v2



jmp mbr_end.entry


.loadstage2_msg: db "Loading (stage 2) bootloader...", 0

%include "bootloader/bios_disk.asm"
%include "bootloader/print.asm"

times 510 - ($-$$) db 0  
dw 0xaa55

%include "bootloader/gdt.asm"

boot_msg:
.kernel_loaded: db `Kernel loaded!\r\nBooting to protected, then long mode...`, 0
.stage2_loaded: db `Done loading bootloader!\r\nLoading kernel...`, 0




mbr_end:
.entry:
cli
; entering unreal mode
mov bx, boot_msg.stage2_loaded
call bios_print


lgdt [protected_gdt.descriptor]

mov eax, cr0
or al, 1
mov cr0, eax

jmp $+2

mov bx, 0x10
mov ds, bx   
mov es, bx

and al, 0xfe
mov cr0, eax

mov bx, 0
mov ds, bx
mov es, bx
sti

mov esi, _bootloader_stage1_size
mov edi, 0x100000
mov ecx, _kernel_sector_size
call bios_disk.load_sectors_v2

mov bx, boot_msg.kernel_loaded
call bios_print


call detect_arch
;call vbe_init
done:

call enter_longmode
jmp $

%include "bootloader/cpu_check.asm"
%include "bootloader/video.asm"
%include "bootloader/enter_kernel.asm"
times 2048 - ($ - $$) db 0
