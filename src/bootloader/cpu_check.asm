[extern _meminfo]
detect_arch:

; detect CPUID

pushfd    
pushfd    
xor dword [esp], 0x00200000 
pop eax         
xor eax, [esp]  
popfd           
and eax, 0x00200000 
jz .print_cpuid_error

; detect long mode

mov eax, 0x80000001
cpuid 
and edx, 0x0020000000 
jz .print_long_error


; APIC
mov eax, 0x00000001
cpuid
test edx, 0x00000200
jz .print_apic_error

; SSE (you might be able to remove this, I think it's a standard requirement)
test edx, 0x2000000
jz .print_sse_error


mov bx, .no_error
call bios_print


; get memory mappings 
mov ax, 0
mov es, ax
mov ax, _meminfo
mov di, ax

mov eax, 0xe820
mov ebx, 0
mov ecx, 24
mov edx, 0x534d4150
int 0x15

jc .print_mem_error
cmp eax, 0x534D4150
jne .print_mem_error

.mem_detect_loop:
add di, 24
mov [di + 20], dword 1
mov eax, 0xe820
mov ecx, 24

int 0x15
jc .mem_detected
cmp ebx, 0
je .mem_detected
jmp .mem_detect_loop
.mem_detected:

add di,24
mov ecx, 24
mov al, 0
rep stosb

ret


.print_cpuid_error:
mov bx, .cpuid_error
call bios_print
jmp $

.print_long_error:
mov bx, .arch_error
call bios_print
jmp $

.print_apic_error:
mov bx, .apic_error
call bios_print
jmp $

.print_mem_error:
mov bx, .mem_error
call bios_print
jmp $

.print_sse_error:
mov bx, .sse_error
call bios_print
jmp $


.cpuid_error:
  db "Somehow, your CPU doesn't even support the ability to detect its abilities. I'm gonna go out on a limb and say your cpu is too old to run this OS.", 0x0
.arch_error:
 db "This operating system requires a x64 processor.", 0x0
.apic_error:
  db "No APIC support.", 0x0
.mem_error:
  db "Couldn't obtain memory map.", 0x0
.sse_error:
  db "This OS requires the SSE instruction set.", 0x0
.no_error:
  db "CPU meets requirements to boot!", 0x0

