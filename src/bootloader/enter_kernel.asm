[extern kernel_init]
[extern _kernel_stack_loc]
[extern _kernel_page_size]
enter_longmode:
cli

mov al, 0x92
or al, 2
out 0x92, al

;enter 32 bit mode
lgdt [protected_gdt.descriptor]
mov eax, cr0
or eax, 0x1
mov cr0, eax

mov eax, 0x8
mov ds, eax

jmp 0x8:init_longmode

bits 32
init_longmode:
mov ebp, 0xffff
mov esp, ebp
mov ax, PROTECTED_DATA_SEGMENT
mov ds, ax
mov ss, ax
mov es, ax
mov fs, ax
mov gs, ax


mov edi, 0x10000
mov cr3, edi

mov eax, 0
mov ecx, 0x1800     ; bzero 6 pages
rep stosd

mov DWORD [0x10000], 0x11003 ; pml4e[0] = pdpe
mov DWORD [0x11000], 0x12003 ; pdpe[0] = pde
mov DWORD [0x12000], 0x83 ; pde[0] = pte

mov DWORD [0x10ff8], 0x13003 
mov DWORD [0x13ff0], 0x14003
mov DWORD [0x14000], 0x15003

mov eax, 0x100003
mov ebx, 0
mov ecx, _kernel_page_size
.kernel_load_loop:
mov DWORD [0x15000 + ebx], eax
add ebx, 8
add eax, 0x1000
loop .kernel_load_loop




mov eax, cr4         
; PAE, OSFXSR, OSXMMEXCPT
or eax, 1 << 5 | 1 << 9 | 1 << 10
mov cr4, eax         

;end of setting up pages
;testing to see if NX bit is available. 
;If it's not and we enable it, it will cause pagefaults on read
mov eax, 0x80000001
cpuid
mov ebx, edx
and ebx, 1 << 20
shr ebx, 9
  
mov ecx, 0xc0000080
rdmsr
or eax, 1 << 8
or eax, ebx
wrmsr

mov eax, cr0
or eax, 1 << 31 | 1 << 0; 
and ax, ~(1 << 2)
mov cr0, eax


lgdt [long_gdt.descriptor]

jmp LONG_CODE_SEGMENT:enter_kernel
enter_kernel:
bits 64
mov rbp, 0
mov rsp, _kernel_stack_loc
mov rax, QWORD kernel_init
jmp rax
full_stop:
cli
hlt
jmp full_stop
ret
