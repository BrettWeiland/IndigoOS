;TODO fix null problem, allow passing value insted of pointer
bios_print:
pusha
mov ah, 0x0e 

.print_loop:
mov al, [bx]          
cmp al, 0             
je .fini   

int 0x10              
inc bx               
jmp .print_loop   

.fini:
mov al, 0xd           
int 0x10
mov al, 0xa           
int 0x10
popa
ret
