#ifndef acpi_included
#define acpi_included
#include <stdint.h>
#include <stdbool.h>
#include <int.h>


//sdt types
#define SDT_MADT 0
#define SDT_HPET 1


void find_root_sdp();
void *find_sdt(int type);
void debug_acpi();

typedef struct sdt {
  char sig[4];
  uint32_t length;
  uint8_t revision;
  uint8_t checksum;
  char OEMID[6];
  char OEMTableID[8];
  uint32_t OEMRevision;
  uint32_t creator_id;
  uint32_t creator_revision;
} __attribute__((packed)) sdt_head;


#endif
