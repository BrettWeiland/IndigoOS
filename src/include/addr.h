#ifndef ADDR_INCLUDED
#define ADDR_INCLUDED

#include <stdint.h>

//couldn't get symbols working, fix later
#define PA_OFFSET                 0xffff800000000000
#define TXT_OFFSET                0xffffffff80000000
#define PHYS_TO_VIRT(addr) ((void *)((uintptr_t)(addr) | PA_OFFSET))

#endif
