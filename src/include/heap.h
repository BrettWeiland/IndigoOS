void *malloc(size_t size);
void *realloc(void *old_chunk, size_t size);
void malloc_init();
void debug_heap();
void free(void *size);
