#ifndef io_header
#define io_header

#include <stdint.h>
#include <printf.h>

static inline void outb(uint16_t port, uint8_t value) {
  asm volatile("outb %1, %0" :: "a"(value), "Nd"(port));
}

static inline uint8_t inb(uint16_t port) {
  uint8_t ret;
  asm volatile("inb %0, %1" : "=a"(ret) : "Nd"(port));
  return(ret);
}

static inline void io_wait() {
  asm volatile("outb 0x80, %%al"::"a"(0));
}

static inline uint64_t read_tsc() {
  uint64_t time;
  asm volatile("xor rax, rax\n"
      "rdtsc\n"
      "shl rdx, 32\n"
      "or rax, rdx\n"
      "mov %0, rax\n"
      :"=r"(time):);
  return time;

}

void outb_wait(uint16_t port, uint8_t value);
void read_msr(uint32_t addr, uint64_t *value);
void write_msr(uint32_t addr, uint64_t value);
void hlt();
uint64_t get_rip();
#endif
