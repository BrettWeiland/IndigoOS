#ifndef isv_defined
#define isv_defined

//TODO move to int.h

struct int_frame {
  void *rip;
  uint64_t segment_selector;
  uint64_t rflags;
  void *rsp;
  uint64_t stack_segment;
} __attribute__((packed));

struct exception_frame {
  uint64_t err;
  struct int_frame frame;
} __attribute__((packed));

__attribute__((interrupt)) void spurrious_int(void *unused);
__attribute__((interrupt)) void kernel_block(void *unused);
__attribute__((interrupt)) void fatal_hwexception(struct int_frame *frame);
__attribute__((interrupt)) void fatal_hwexception_errcode(struct exception_frame *frame);
__attribute__((interrupt)) void lapic_timer_racefixer(void *unused);

#endif
