#ifndef _SERIAL_H_
#define _SERIAL_H_

#include <stdint.h>
#include <stdbool.h>





void init_klog();
void serial_out(uint16_t port, char *string);
void _putchar_serial(uint16_t port, char character);
void move_cursor(unsigned int x, unsigned int y);


void clear_screen();


#endif
