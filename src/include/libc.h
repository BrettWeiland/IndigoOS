#ifndef _STRING_H_
#define _STRING_H_
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

void *strcpy(char *dest, char *src);
void *memcpy(void *dest, void *src, size_t n);
void *bzero(void *dest, size_t size);
bool *is_empty(void *dest, size_t size);
void *memset(void *s, char c, size_t n);
size_t strlen(const char *s);
int strcmp(const char *str1, const char *str2);
int strncmp(const char *str1, const char *str2, size_t n);
int memcmp(const void *s1, const void *s2, size_t n);
int ceil(double n1);
int round(double n1);

#endif
