#ifndef madt_header
#define madt_header
#include <int.h>

struct cores_info {
  unsigned int corecount;
  uint8_t apic_id[256];
  uint8_t bsp;
};

void init_madt();
void get_ioapic(struct ioapic_fixedlen *ret);
void get_coreinfo(struct cores_info *cores);
void debug_madt();
unsigned int irq_to_gsi(unsigned int irq);
lapic_t get_lapic();
void boot_multicore();


#endif
