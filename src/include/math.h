#ifndef MATH_INCLUDED
#define MATH_INCLUDED

#define DIV_ROUND_UP(number, factor) (((number) + ((factor) - 1)) / factor)

#endif
