#ifndef _PAGE_H_
#define _PAGE_H_ 

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>



void unmap_lowmem();
size_t map_complete_physical();
void debug_pzone();
void init_pmap_smp();


struct phys_map *init_pmap(size_t pagetable_size);
void *palloc(size_t size); 
void pfree(void *addr, size_t size);
void debug_pmap();
void get_mem_capabilities();
void ram_stresser();
void fix_stack();


#endif
