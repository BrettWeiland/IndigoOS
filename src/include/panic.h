#ifndef PANIC_INCLUDED
#define PANIC_INCLUDED

#include <stdint.h>

#define EASTEREGG_BLOATWARE

//TODO this needs to be moved once we find somehwere better
struct stack_frame {
  struct stack_frame *next; //rbp
  void *function_base;      //rip
} __attribute__((packed));

struct registers {
  uint64_t rax, rbx, rcx, rdx, rsi, rdi;
} __attribute__((packed));

void panic(int reason, void *frame_p, struct registers *regs);


#define PANIC(reason) panic(reason, 0, 0)

//hardware exceptions/faults
//kernel panic reasons 0-31 reserved for these
//later on, we can make an ugly function to find our vector.
//I did a lot of work assuming it was pushed onto the stack,
//so if I choose not to impliment that these defs will be deleted.
/**
#define KERNEL_PANIC_HWE_DE               0
#define KERNEL_PANIC_HWE_BR               5
#define KERNEL_PANIC_HWE_UD               6
#define KERNEL_PANIC_HWE_NM               7
#define KERNEL_PANIC_HWE_DF               8
#define KERNEL_PANIC_HWE_TS               10
#define KERNEL_PANIC_HWE_NP               11
#define KERNEL_PANIC_HWE_SS               12
#define KERNEL_PANIC_HWE_GP               13
#define KERNEL_PANIC_HWE_PF               14
#define KERNEL_PANIC_HWE_MF               16
#define KERNEL_PANIC_HWE_AC               17
#define KERNEL_PANIC_HWE_MC               18
#define KERNEL_PANIC_HWE_XM               19
#define KERNEL_PANIC_HWE_VE               20
#define KERNEL_PANIC_HWE_SE               30
**/
#define KERNEL_PANIC_HW_EXCEPTION         0
#define KERNEL_PANIC_HW_EXCEPTION_ERR     1 //exception with error code
//kernel generated exceptions
#define KERNEL_PANIC_RSDP_UNFOUND         32
#define KERNEL_PANIC_KERNEL_RETURNED      33
#define KERNEL_PANIC_INVALID_PFREE        34
#define KERNEL_PANIC_INVALID_RSDT         35
#define KERNEL_PANIC_INVALID_IOAPIC_VEC   36
#define KERNEL_PANIC_HPET_REQUIRED        37
#define KERNEL_PANIC_SMP_FAILED           38
#define KERNEL_PANIC_PALLOC_TOO_LARGE     39

#define DEV_EMAIL "brett_weiland@bpcspace.com"




#endif
