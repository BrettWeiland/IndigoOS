#ifndef SMP_INCLUDED
#define SMP_INCLUDED
#include <stdint.h>
#include <stdbool.h>
#include <cpuid.h>
#include <printf.h>
void smp_prepare();
extern uint8_t corecount;

/**
static inline void lock(uint8_t *lock) {
  asm("mov al, 1\n"
      "spinlock:\n"
      "lock xchgb [%0], al\n"
      "test al, al\n"
      "pause\n"
      "jnz spinlock\n"
      ::"r"(lock):"al");
} 
**/

static inline void lock(uint16_t *mutex) {
  asm(".spinlock_%=:\n"
      "lock bts %0, 0\n"
      "jnc .done_%=\n"
      "pause\n"
      "jmp .spinlock_%=\n"
      ".done_%=:\n"
      ::"m"(*mutex));
}

static inline void unlock(uint16_t *mutex) {
  asm("lock btr %0, 0\n"
      ::"m"(*mutex));
}

/**
static inline void unlock(uint8_t *lock) {
  asm("lock andb [%0], 0"::"r"(lock));
}
**/

static inline bool get_set_mutex(uint16_t *mutex) {
  bool ret;
  asm("lock bts %1, 0\n"
      "jc .mutex_taken_%=\n"
      "mov %0, 0\n"
      "jmp .done_%=\n"
      ".mutex_taken_%=:\n"
      "mov %0, 1\n"
      ".done_%=:\n"
      :"=r"(ret)
      :"m"(*mutex));
  return ret;
}

//THIS IS ONLY UNTIL WE GET MULTITHREADING SET UP
uint8_t get_coreid();


#endif
