#ifndef timer
#define timer

void init_timer();
void usleep(unsigned int us);
uint64_t timestamp();

#endif
