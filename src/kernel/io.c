#include <io.h>
#include <stdint.h>
//these functions are assumed not to be time-sensitive.
void outb_wait(uint16_t port, uint8_t value) {
  outb(port, value);
  io_wait(); 
}

void read_msr(uint32_t addr, uint64_t *value) {
  uint32_t low, high;
  asm volatile("rdmsr" : "=a"(low), "=d"(high) : "c"(addr));
  *value = low | ((uint64_t)high << 32);
}

void write_msr(uint32_t addr, uint64_t value) {
  uint32_t low = value & UINT32_MAX;
  uint32_t high = value >> 32;
  asm volatile("wrmsr"::"a"(low), "d"(high), "c"(addr));
}

void hlt() {
  asm("cli\nhlt");
}

