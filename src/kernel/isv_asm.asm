global timer_init_pit
global kernel_block
global fatal_hwexception
global fatal_hwexception_errcode
global lapic_timer_racefixer

extern clear_int
extern panic
extern calibrate_lapic_waiting

;I need to get my timing right and consistently boot smp,
;_then_ I'll work on cleaning this up.

timer_init_pit:
cli
mov ecx, DWORD [rbx + 0x390]
mov DWORD [rbx + 0x380], 0
in al, 0x61
and al, 0xfe
out 0x61, al
mov DWORD [rbx + 0xb0], 0
iretq


kernel_block:
call clear_int
iretq


fatal_hwexception:
push qword rdi
push qword rsi
push qword rdx
push qword rcx
push qword rbx
push qword rax
mov rdi, 0
mov rdx, rsp
mov rsi, rdx
add rsi, 48
call panic

; rdi: reason
; rsi: frame
; rdx: regs

; 0xffff80000010af70
fatal_hwexception_errcode:
push qword rdi
push qword rsi
push qword rdx
push qword rcx
push qword rbx
push qword rax
mov rdi, 1
mov rdx, rsp
mov rsi, rdx
add rsi, 48
call panic

lapic_timer_racefixer: ;in case the lapic makes it to zero, impliment me
inc rbx
call clear_int
mov QWORD [rsp], calibrate_lapic_waiting
iretq
