#include <stdbool.h>
#include <smp.h>
#include <klog.h>
#include <printf.h>
#include <paging.h>
#include <video.h>
#include <acpi.h>
#include <panic.h>
#include <addr.h>
#include <stdbool.h>
#include <int.h>
#include <io.h>
#include <cpuid.h>
#include <heap.h>
#include <random.h>
#include <timer.h>
#include <libc.h>
#include <smp.h>

//testing headers
#include <smp_racetest.h>


void kmain() {
  printf("Kernal started on core %i\n", get_coreid()); 
  racetest();
  PANIC(KERNEL_PANIC_KERNEL_RETURNED);
}

static bool smp_unlocked = false;
void smp_kinit() {

  asm(".wait_for_release:\n"
      "mov al, [%0]\n"
      "test al, al\n"
      "jz .wait_for_release\n"
      ::"m"(smp_unlocked));
      
  smp_load_idt();
  kmain();
}

//TODO move to global constructors
void kernel_init() {
  size_t pmap_size; 

  get_mem_capabilities(); 
  pmap_size = map_complete_physical();
  init_klog();
  init_pmap(pmap_size); 
  printf("\nKernal started on core 1!\n");
 
  
  find_root_sdp();
  debug_acpi();
  
  init_interrupts_bsp();

  
  randinit();

  clear_screen();
  debug_pzone();
  smp_prepare();

  //the rest of this needs to get done before the cores start executing
  init_pmap_smp();



  smp_unlocked = true;

  fix_stack();
  unmap_lowmem();
  kmain();
}
