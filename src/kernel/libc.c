#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
// TODO clean up variable names
int strncmp(const char *s1, const char *s2, unsigned int n) {
  int i;
  for(i = 0; ((i < (n - 1)) && (s1[i] != '\0') && (s2[i] != '\0')); i++) {
    if(s1[i] != s2[i]) {
      return(s1[i] - s2[i]);
    }
  }
  return(s1[i] - s2[i]);
}

//this one hasn't been tested
size_t strlen(const char *s) {
  size_t len = 0;
  while(s[len + 1] != '\0') len++;
  return(len);
}

bool is_empty(const char *s, size_t size) {
  for(size_t i = 0; i < size; i++) {
    if(s[i]) return false;
  }
  return true;
}

int strcmp(const char *s1, const char *s2) {
  int i;
  for(i = 0; ((s1[i] != '\0') && (s2[i] != '\0')); i++) {
    if(s1[i] != s2[i]) {
      return(s1[i] - s2[i]);
    }
  }
  return(s1[i] - s2[i]);
}

int memcmp(const void *s1, const void *s2, size_t n) {
  const unsigned char *p1 = s1;
  const unsigned char *p2 = s2;
  int i;
  for(i = 0; i < n - 1; i++) {
    if(p1[i] != p2[i]) {
      return(p1[i] - p2[i]);
    }
  }
  return(p1[n-1] - p2[n-1]);
}

void strcpy(char *dest, char *src) {
  for(unsigned int i = 0; src[i] != '\0'; i++){
    dest[i] = src[i];
  }
}

void *memcpy(void *dest, char *src, size_t n) {
  char *p = dest;
  for(unsigned int i = 0; i < n; i++) {
    p[i] = src[i];
  }
  return(dest);
}

void *bzero(void *dest, size_t size) {
  char *p = dest;
  for(uint64_t i = 0; i < size; i++) {
    p[i] = 0;
  }
  return(dest);
}

//TODO move this function to a seperate math library
int ceil(double n) {
  int low_n = (int)n;
  if(n == (double)low_n) return(low_n);
  return(low_n + 1);
}

int round(double n) {
  return(int)(n + 0.5);
}

void *memset(void *s, char c, size_t n) {
  char *p = s;
  for(size_t i = 0; i < n; i++) {
    p[i] = c;
  }
  return(s);
}
