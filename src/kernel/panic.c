#include <panic.h>
#include <stdint.h>
#include <printf.h>
#include <addr.h>
#include <klog.h>
#include <isv.h>
#include <smp.h>

static uint16_t panic_lock = 0;

void panic(int reason, void *eframe_p, struct registers *regs) { // will fill with debugging info latter
  lock(&panic_lock);
  clear_screen();
  
#ifdef EASTEREGG_BLOATWARE
  printf("\nKernel PANIC!!!!!!!\n");
  printf(
"     _.-^^---....,,--       \n"
" _--                  --_  \n"
"<                        >)\n"
"|          BOOM           | <------- your computer\n"
" \\._                   _./  \n"
"    ```--. . , ; .--'''       \n"
"          | |   |             \n"
"       .-=||  | |=-.   \n"
"       `-=#$%&%$#=-'   \n"
"          | ;  :|     \n"
" _____.,-#%&$@%#&#~,._____\n");
#else
  printf("Kernel Panic!\n");
#endif

  switch(reason) {
    case KERNEL_PANIC_HW_EXCEPTION:
    case KERNEL_PANIC_HW_EXCEPTION_ERR:
      printf("\nHardware exception in kernel space!\n");
      break;
    
    case KERNEL_PANIC_RSDP_UNFOUND:
      printf("\nRSDP unfound!\n");
      break;
    case KERNEL_PANIC_KERNEL_RETURNED:
      printf("\nThe kernel (almost) reached its return!\n");
      break;
    case KERNEL_PANIC_INVALID_PFREE:
      printf("\npfree was called with invalid paramaters.\n");
      break;
    case KERNEL_PANIC_INVALID_RSDT:
      printf("\nInvalid RSDT or XSDT after RSDP verification.\nIf this happens, go buy some lottery tickets.\n");
      break;
    case KERNEL_PANIC_INVALID_IOAPIC_VEC:
      printf("\nThe kernel tried to make a vector that was too high for the IOAPIC to handle.\n");
      break;
    case KERNEL_PANIC_HPET_REQUIRED:
      printf("\nHPET is required. \nIf you get this error, let know;\nif enough people share this issue, I'll impliment PIT usage.\n");
    case KERNEL_PANIC_SMP_FAILED:
      printf("\nNot all cores booted successfully (see text before panic).\n");
      break;
    case KERNEL_PANIC_PALLOC_TOO_LARGE:
      printf("\npalloc was called with a size greater then supported.\n");
      break;
    default:
      printf("\nUnknown panic code %i\n.", reason);
      break;
  }
  struct stack_frame *frame;

  char reg_str[156];
  if((reason == KERNEL_PANIC_HW_EXCEPTION) || (reason == KERNEL_PANIC_HW_EXCEPTION_ERR)) {
    sprintf(reg_str, "rax:\t0x%lx\n"
        "rbx:\t0x%lx\n"
        "rcx:\t0x%lx\n"
        "rdx:\t0x%lx\n"
        "rsi:\t0x%lx\n"
        "rdi:\t0x%lx\n", regs->rax, regs->rbx, regs->rcx, regs->rdx, regs->rsi, regs->rdi);
  }

  if(reason == KERNEL_PANIC_HW_EXCEPTION) {
    struct int_frame *ex_frame = eframe_p;
    printf("Offending instruction:\t0x%p\n", ex_frame->rip);
    printf(reg_str);
  }
  else if (reason == KERNEL_PANIC_HW_EXCEPTION_ERR) {
    struct exception_frame *ex_frame = eframe_p;
    printf("Offending instruction: 0x%p\nError value: %x\n", ex_frame->frame.rip, ex_frame->err);
    printf(reg_str);
  }
  asm("mov %0, rbp" : "=r"(frame) ::);
  printf("\nCall trace:\n");
  
  for(; frame->next != 0; frame = frame->next) {
    printf("\t0x%p\n", frame->function_base);
  }
  

  //It's not public yet, but if I ever get somewhere it will be
  printf("\nAfter ensuring your computer meets the requirements specified, if you think this is a bug, please open an issue on the git repo or email me at %s.\n", DEV_EMAIL);
  asm("cli\nhlt");

}
