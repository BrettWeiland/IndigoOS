#include <stdbool.h>
#include <cpuid.h>
#include <printf.h>
#include <stdint.h>

static bool hw_random = false;
static unsigned long int seed = -1;

void randinit() {
  unsigned int unused, ecx;
  ecx = 0;
  __get_cpuid(1, &unused, &unused, &ecx, &unused); 
  hw_random = (ecx >> 30) & 1;
  printf("Kernel random source: %s.\n", (hw_random) ? "rdrand" : "pseudo");
}

//As of now, this function isn't nessesarily meant to be fast.
unsigned int randint() {
  uint64_t random_long = 0;
  if(hw_random) {
    asm("rand:\n"
        "rdrand %0\n"
        "jc finished\n"
        "pause\n"
        "jmp rand\n"
        "finished:" :"=r"(random_long)::"rax" );
  }
  else {
    //TODO FUCKING ENTROPY!!!!!!!111
    seed = 1103515245 * seed + 12345;
    return(unsigned int)(seed / 65536) % 32768;
  }
  return random_long;
}

