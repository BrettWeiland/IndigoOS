//Disregard bad code here.
//I'm going to delete this whole file once I am confident smp is safe.

#define CHUNK_SIZE_FROM_INDEX(i) ((1 << ((i) + 5)))

#include <printf.h>
#include <heap.h>
#include <libc.h>
#include <random.h>
#include <smp.h>
#include <paging.h>

//will delete later

static uint16_t lockeroni = 0;

void test_malloc(unsigned int cnt) {
  void *testchunks[cnt];
  unsigned int rindex[cnt], testchunk_size, i, x;

  bzero(rindex, cnt * sizeof(unsigned int));
  for(x = 0; x < cnt; x++) {
    testchunk_size = (CHUNK_SIZE_FROM_INDEX(randint() % 7) - 24);
    testchunks[x] = malloc(testchunk_size);
    printf("ALLOCATING CHUNK %p SIZE %i\n", (void *)testchunks[x] - 24, testchunk_size);
  }
  for(x = 0; x < cnt;) {
    i = randint() % cnt;
    if(rindex[i]) continue;
    rindex[i] = x;
    x++;
  }

  for(x = 0; x < cnt; x++) {
    //printf("FREEING CHUNK %p\n", (void *)testchunks[rindex[x]]);
    free(testchunks[rindex[x]]);
  }

  printf("\nmalloc tester:\n");
  printf("THIS NEEDS TO BE EMPTY______________\n");
  debug_heap();
  printf("____________________________________\n");
  unlock(&lockeroni);
}

#define DEBUG_CORE_CNT 2

uint8_t cores_waiting = DEBUG_CORE_CNT;
uint8_t cores_waiting_2 = DEBUG_CORE_CNT;
uint8_t cores_waiting_3 = DEBUG_CORE_CNT;
uint8_t cores_waiting_4 = DEBUG_CORE_CNT;
uint8_t cores_waiting_5 = DEBUG_CORE_CNT;
void *smp_outputs[DEBUG_CORE_CNT];

void racetest() {
  uint8_t core_id = get_coreid();
  uint8_t c_check;
  unsigned int core_i; 

  asm("lock decb [%0]\n"
      "spinlock_%=:\n"
      "cmpb [%0], 0\n"
      "jnz spinlock_%=\n"
      ::"m"(cores_waiting));

  smp_outputs[core_id] = palloc(0x1000);
  printf("Make sure none of these match (palloc) -> %lx\n", smp_outputs[core_id]);
  free(smp_outputs[core_id]);
  
  asm("lock decb [%0]\n"
      "spinlock_%=:\n"
      "cmpb [%0], 0\n"
      "jnz spinlock_%=\n"
      ::"m"(cores_waiting_2));

  if(core_id == 0) {
    for(core_i = 0; core_i < DEBUG_CORE_CNT; core_i++) {
      for(c_check = core_i + 1; c_check < DEBUG_CORE_CNT; c_check++) {
        if(smp_outputs[core_i] == smp_outputs[c_check]) {
          printf("TEST FAILED\n");
          for(;;);
        }
      }
    }
    printf("TEST PASSED\n");
    printf("malloc beforehand: \n");
    debug_heap();
  }
  

  asm("lock decb [%0]\n"
      "spinlock_%=:\n"
      "cmpb [%0], 0\n"
      "jnz spinlock_%=\n"
      ::"m"(cores_waiting_3));


  smp_outputs[core_id] = malloc(1);
  printf("Make sure none of these match (malloc) -> %lx\n", smp_outputs[core_id]);

  asm("lock decb [%0]\n"
      "spinlock_%=:\n"
      "cmpb [%0], 0\n"
      "jnz spinlock_%=\n"
      ::"m"(cores_waiting_4));

  if(core_id == 0) {
    for(core_i = 0; core_i < DEBUG_CORE_CNT; core_i++) {
      for(c_check = core_i + 1; c_check < DEBUG_CORE_CNT; c_check++) {
        if(smp_outputs[core_i] == smp_outputs[c_check]) {
          printf("TEST FAILED\n");
          for(;;);
        }
      }
    }
    printf("TEST PASSED\n");
    printf("malloc afterhand: \n");
    debug_heap();
  }
}
