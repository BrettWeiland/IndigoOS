#include <printf.h>
#include <video.h>

//to be implimented when paging is set up

void dump_video() {
  struct mode_info *video = (struct mode_info *)0x600;
  printf("Video info:\nx:\t%u\ny:\t%u\nbbp:\t%u\nloc:\t0x%p\n", 
      video->width, video->height, video->bpp, video->framebuffer);
}
